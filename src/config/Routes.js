import React from 'react';

import {Router, Scene, Stack} from 'react-native-router-flux';

import Login from '../pages/Login';
import Dashboard from '../pages/Dashboard';

export default props => (
	<Router>
		<Stack key="root" hideTabBar titleStyle={{alignSelf: 'center'}}>
			<Scene hideNavBar key="Login" component={Login} title="Tickets - Dev::Rockeatseat"/>
			<Scene hideNavBar key="Dashboard" component={Dashboard} title="Tickets - Dashboard"/>
		</Stack>
	</Router>
);