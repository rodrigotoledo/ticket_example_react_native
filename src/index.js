/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore} from 'redux';

import Routes from './src/config/Routes';
import reducers from './Reducers';

import Login from './pages/Login';

const App = () => <Login/>;

export default App;