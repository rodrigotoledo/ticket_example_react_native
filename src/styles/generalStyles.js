import { StyleSheet } from 'react-native';

const generalStyles = StyleSheet.create({
  center:{
    justifyContent : 'center',
    alignItems : 'center'
  }
});

export default generalStyles;