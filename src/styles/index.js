import colors from './colors';
import fonts from './fonts';
import generalStyles from './generalStyles';

export { colors, fonts };
