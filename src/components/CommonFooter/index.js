import React from 'react';

import { Text } from 'react-native';
import { FooterTab, Footer, Button } from 'native-base';

const CommonFooter = () => (
  <Footer>
    <FooterTab>
      <Button full>
        <Text>Dev.: Comunidade Rocketseat</Text>
      </Button>
    </FooterTab>
  </Footer>
);

export default CommonFooter;
