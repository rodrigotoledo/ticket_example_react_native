import React from 'react';

import { Text } from 'react-native';
import { Header, Body, Icon } from 'native-base';

import generalStyles from '../../styles/generalStyles';

const CommomHeader = () => (
  <Header style={generalStyles.center}>
    <Icon name='md-paper' style={{color: '#ffffff', marginRight: 4,}} />
    <Text style={{color:'#ffffff', fontSize: 16,}}>Tickets - Dev::Rockeatseat</Text>
  </Header>
);

export default CommomHeader;
