import React, { Component } from 'react';

import { Text, View } from 'react-native';
import { Header, Container, Content, Form, Item, Input, Label, Icon, Button, Title } from 'native-base';

import CommonHeader from '../../components/CommonHeader';
import CommonFooter from '../../components/CommonFooter';

import styles from './styles';
import colors from '../../styles/colors';

export default class Dashboard extends Component {
	state = {};

	render() {
		return (
			<Container>
				<CommonHeader />
				<Content padder contentContainerStyle={styles.content}>
					<Icon name="md-paper" style={styles.icon} />
					<Text>Preencha os campos abaixo para efetuar o login</Text>
					<Form>
						<Item floatingLabel style={styles.input}>
							<Label>Usuário</Label>
							<Input />
						</Item>
						<Item floatingLabel style={styles.input}>
							<Label>Senha</Label>
							<Input />
						</Item>
						<Button block style={styles.button}>
							<Text style={styles.buttonText}>Entrar</Text>
						</Button>
					</Form>
				</Content>
				<CommonFooter />
			</Container>
		);
	}
}
