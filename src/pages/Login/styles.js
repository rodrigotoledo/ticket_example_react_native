import { StyleSheet } from 'react-native';
import { colors } from '../../styles';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
  },
  icon: {
    fontSize: 60,
    alignSelf: 'center',
  },
  input: {
    marginHorizontal: 10,
    marginVertical: 0,
  },
  button: {
    marginVertical: 20,
    marginHorizontal: 10,
  },
  buttonText: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: 18,
  },
});

export default styles;
