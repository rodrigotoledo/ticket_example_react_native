let INITIAL_STATE = {
	user_email: '',
	user_password: '',
};
export default (state = INITIAL_STATE, action) => {
	if (action.type == 'change_user_email') {
		return { ...state, user_email: action.payload }	
	}
	if (action.type == 'change_user_password') {
		return { ...state, user_password: action.payload }	
	}

	return state;
}