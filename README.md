# INSTRUCOES PARA UTILIZACAO DO PROJETO

Para que o projeto seja executado corrementa voce pode seguir os passos abaixo

## Detalhes para instalacao do projeto react-native:

* Baixar o projeto e entrar na pasta do mesmo

* Instalar as bibliotecas que estão listadas no arquivo **package.json**, mas caso falte algo vamos procurar no google e ver o que precisamos.

``npm install``

``npm install --save firebase``

``npm install --save native-base``

``npm install --save react-native-router-flux``

``npm install --save react-native-vector-icons``

``npm install --save redux``

``npm install --save react-redux``

``react-native link``

* Bom agora é simples, vamos rodar nosso aplicativo com o comando

``react-native run-android``

ou

``react-native run-ios``

### Pronto tudo OK!

**PS: O PROJETO ESTÁ EM PLENO DESENVOLVIMENTO ENTÃO ÍCONES, MELHORIAS VISUAIS ISSO TUDO AINDA NÃO ESTÁ FECHADO**